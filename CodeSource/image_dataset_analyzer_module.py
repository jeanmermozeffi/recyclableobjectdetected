import os
import cv2
import numpy as np
import pandas as pd


class ImageDatasetAnalyzer:
    def __init__(self, data_set_path):
        self.data_set_path = data_set_path
        self.classes = []
        self.num_images_per_class = {}
        self.total_width = 0
        self.total_height = 0
        self.num_images = 0
        self.file_types = set()
        self.min_sizes_per_class = {}
        self.max_sizes_per_class = {}
        self.min_size_overall = None
        self.max_size_overall = None

        # Analyser les tailles des images au moment de initialisation
        self.initialize()

    def initialize(self):
        for class_name in os.listdir(self.data_set_path):
            class_path = os.path.join(self.data_set_path, class_name)
            if os.path.isdir(class_path):
                self.classes.append(class_name)
                num_images_in_class = 0
                class_min_width = float('inf')
                class_min_height = float('inf')
                class_max_width = 0
                class_max_height = 0

                for image_file in os.listdir(class_path):
                    image_path = os.path.join(class_path, image_file)
                    img = cv2.imread(image_path)
                    height, width, _ = img.shape
                    self.total_width += width
                    self.total_height += height
                    self.num_images += 1

                    file_extension = os.path.splitext(image_file)[-1].lower()
                    self.file_types.add(file_extension)

                    num_images_in_class += 1

                    class_min_width = min(class_min_width, width)
                    class_min_height = min(class_min_height, height)
                    class_max_width = max(class_max_width, width)
                    class_max_height = max(class_max_height, height)

                self.num_images_per_class[class_name] = num_images_in_class
                self.min_sizes_per_class[class_name] = (class_min_width, class_min_height)
                self.max_sizes_per_class[class_name] = (class_max_width, class_max_height)

        self.min_size_overall = (self.total_width / self.num_images, self.total_height / self.num_images)
        self.max_size_overall = (max(self.max_sizes_per_class.values(), key=lambda x: max(x)), max(self.max_sizes_per_class.values(), key=lambda x: max(x)))

    def get_min_max_image_size_per_class(self):
        return self.min_sizes_per_class, self.max_sizes_per_class

    def get_min_max_image_size_overall(self):
        return self.min_size_overall, self.max_size_overall

    def get_min_image_size(self):
        return self.min_size_overall

    def get_max_image_size(self):
        return self.max_size_overall

    def get_average_image_size(self):
        average_width = self.total_width / self.num_images
        average_height = self.total_height / self.num_images
        return int(average_width), int(average_height)

    def get_total_num_images(self):
        return self.num_images

    def get_class_distribution(self):
        return self.num_images_per_class

    def get_file_types(self):
        return list(self.file_types)

    def build_data_frame(self):
        average_width, average_height = self.get_average_image_size()

        data = {
            'Classes': self.classes,
            'Total images': [self.get_total_num_images()],
            'Taille moyenne': [f"{int(average_width)}x{int(average_height)} pixels"],
            'Types fichiers': [', '.join(self.get_file_types())]
        }
        data.update({f'{class_name} length': [num_images_in_class] for class_name, num_images_in_class in self.get_class_distribution().items()})

        # Ajoutez les colonnes pour les tailles minimales et maximales par classe
        data.update({f'{class_name} min size': [f"{int(min_w)}x{int(min_h)} pixels"] for class_name, (min_w, min_h) in self.min_sizes_per_class.items()})
        data.update({f'{class_name} max size': [f"{int(max_w)}x{int(max_h)} pixels"] for class_name, (max_w, max_h) in self.max_sizes_per_class.items()})

        # Ajoutez les colonnes pour les tailles minimales et maximales globales
        min_overall_width, min_overall_height = self.min_size_overall
        max_overall_width, max_overall_height = self.max_size_overall
        data['Taille min globale'] = [f"{int(min_overall_width)}x{int(min_overall_height)} pixels"]
        data['Taille max globale'] = [f"{int(max_overall_width[0])}x{int(max_overall_height[0])} pixels"]

        # Convertissez les colonnes qui ont deux éléments ou plus en une seule chaîne
        for column in data:
            if isinstance(data[column], list) and len(data[column]) >= 2:
                data[column] = ', '.join(map(str, data[column]))

        data_frame = pd.DataFrame(data)

        return data_frame


# if __name__ == "__main__":
#     data_set_path = '/Users/jeanmermozeffi/Documents/CYCLE INGENIEUR TEK/CYCLE INGENIEUR 3/Stage Ete/Waste Classification Dataset/dataset'
#     analyzer = ImageDatasetAnalyzer(data_set_path)
#
#     print("========== Les détail du dossiers ===========")
#     print(analyzer.build_data_frame())

