import os
import cv2
import numpy as np
import pandas as pd


def analyze_image_dataset(dataset_path):
    dataset_info = {}
    classes = []
    num_images_per_class = {}
    total_width = 0
    total_height = 0
    num_images = 0
    file_types = set()

    for class_name in os.listdir(dataset_path):
        class_path = os.path.join(dataset_path, class_name)
        if os.path.isdir(class_path):
            classes.append(class_name)
            num_images_in_class = 0

            for image_file in os.listdir(class_path):
                image_path = os.path.join(class_path, image_file)
                img = cv2.imread(image_path)
                height, width, _ = img.shape
                total_width += width
                total_height += height
                num_images += 1
                num_images_in_class += 1

                file_extension = os.path.splitext(image_file)[-1].lower()
                file_types.add(file_extension)

            num_images_per_class[class_name] = num_images_in_class

    average_width = total_width / num_images
    average_height = total_height / num_images

    dataset_info['classes'] = classes
    dataset_info['num_images_per_class'] = num_images_per_class
    dataset_info['total_num_images'] = num_images
    dataset_info['average_image_size'] = (average_width, average_height)
    dataset_info['file_types'] = list(file_types)

    return dataset_info


dataset_path = '/Users/jeanmermozeffi/Documents/CYCLE INGENIEUR TEK/CYCLE INGENIEUR 3/Stage Ete/Waste Classification Dataset/dataset'
info = analyze_image_dataset(dataset_path)

data = {
    'Classes': info['classes'],
    'Total images': [info['total_num_images']],
    'Taille moyenne': [f"{int(info['average_image_size'][0])}x{int(info['average_image_size'][1])} pixels"],
    'Types fichiers': [', '.join(info['file_types'])]
}

# Ajoutez les colonnes pour le nombre d'images par classe
for class_name, num_images_in_class in info['num_images_per_class'].items():
    data[f'{class_name} length'] = [num_images_in_class]

# Convertissez les colonnes qui ont deux éléments ou plus en une seule chaîne
for column in data:
    if isinstance(data[column], list) and len(data[column]) >= 2:
        data[column] = ', '.join(map(str, data[column]))

df = pd.DataFrame(data)
df






#%%
